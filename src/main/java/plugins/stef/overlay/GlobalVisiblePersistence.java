/**
 * 
 */
package plugins.stef.overlay;

import icy.gui.main.GlobalSequenceListener;
import icy.main.Icy;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginDaemon;
import icy.sequence.Sequence;

import org.w3c.dom.Node;

import plugins.stef.roi.bloc.overlay.GlobalVisibleOverlay;

/**
 * @author Stephane
 */
public class GlobalVisiblePersistence extends Plugin implements PluginDaemon, GlobalSequenceListener
{
    /**
     * Create a Overlay from a XML node.
     * 
     * @param node
     *        XML node defining the overlay
     * @return the created Overlay or <code>null</code> if the Overlay class does not support XML persistence a default
     *         constructor
     */
    public static GlobalVisibleOverlay createFromXML(Sequence sequence, Node node)
    {
        if (node == null)
            return null;

        final GlobalVisibleOverlay result = new GlobalVisibleOverlay();

        result.loadFromXML(sequence, node);

        return result;
    }

    @Override
    public void init()
    {
        for (Sequence sequence : Icy.getMainInterface().getSequences())
            sequenceOpened(sequence);

        Icy.getMainInterface().addGlobalSequenceListener(this);
    }

    @Override
    public void run()
    {
        // nothing here
    }

    @Override
    public void stop()
    {
        Icy.getMainInterface().removeGlobalSequenceListener(this);
    }

    @Override
    public void sequenceOpened(Sequence sequence)
    {
        if (sequence.isNodeExisting(GlobalVisibleOverlay.ID) != null)
        {
            final GlobalVisibleOverlay overlay = createFromXML(sequence, sequence.getNode(GlobalVisibleOverlay.ID));
            if(overlay != null) sequence.addOverlay(overlay);
        }
    }

    @Override
    public void sequenceClosed(Sequence sequence)
    {
        // nothing here
    }
}
